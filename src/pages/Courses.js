import Hero from './../components/Banner';
import CourseCard from './../components/CourseCard';
import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';

const data = {
	title: 'Course Catalog',
	content: 'Browse through our catalog of courses.'
}

export default function Courses () {
	const [courseCollection, setCourseCollection] = useState([]);

	useEffect(() => {
		fetch('https://agile-springs-51885.herokuapp.com/courses').then(res => res.json())
		.then(convertedData => {
			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course.id}courseProp={course} />
				)
			}))
		})
	},[])

	return(
		<>
			<Hero bannerData={data}/>
			
				<Container>
					
						{courseCollection}
					
				</Container>
			
		</>
	);
};