import Hero from '../components/Banner';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext'

const data = {
	title: 'Welcome to the Register Page',
	content: 'Create an account to enroll.'
}

export default function Register () {

	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isMobileValid, setIsMobileValid] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);

	useEffect(() => {
		if ( mobileNo.length === 11)			
		{
			setIsMobileValid(true);
			if (
				password1 === password2 && password1 !== '' && password2 !== ''
			)
			{
				setIsMatched(true);
				if (firstName !== '' && lastName !== '' && email !== '') {
					setIsAllowed(true);
					setIsActive(true);
				} else {
					setIsAllowed(false);
					setIsActive(false);
				}	
			} else {
				setIsMatched(false);
				setIsAllowed(false);
				setIsActive(false);
			}
		} 
		else if (password1 !== '' && password1 === password2) {
			setIsMatched(true);
		}
		else {
			setIsActive(false);
			setIsMatched(false);
			setIsMobileValid(false);
			setIsAllowed(false);
		}
	},[firstName, lastName, email, password1, password2, mobileNo]);

	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault();

		const isRegistered = await fetch('https://agile-springs-51885.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			    firstName: firstName,
			    lastName: lastName,
			    email: email,
			    password: password1,
			    mobileNo: mobileNo
			})
		}).then(response => response.json()).then(dataJSON => {
			console.log(dataJSON);
			if (dataJSON.email) {
				return true;
			} else {
				return false;
			}
		})

		if (isRegistered) {
			setFirstName('');
			setLastName('');
			setEmail('');
			setMobileNo('');
			setPassword1('');
			setPassword2('');

			await Swal.fire({
				icon: "success",
				title: 'Registration Successful!',
				text: 'Thank you for creating an account.'
			})

			window.location.href = "/login";
		} else {
			await Swal.fire({
				icon: "error",
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			})
		}
		
	};

	return (

		user.id
		?
			<Navigate to="/courses" replace={true}/>
		:

		<>
			<Hero bannerData={data}/>
			
			<Container>
				{
					isAllowed ?
						<h1 className="text-center text-success">You May Now Register</h1>
					:
						<h1 className="text-center">Register Form</h1>
				}
				
				<h6 className="text-center mt-3 text-secondary">Fill Up the Form Below</h6>
				
				<Form onSubmit={e => registerUser(e)}>
					<Form.Group>
						<Form.Label>First Name:</Form.Label>
						<Form.Control type="text" placeholder="Juan"
						required
						value={firstName}
						onChange={event => {setFirstName(event.target.value)}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name:</Form.Label>
						<Form.Control type="text" placeholder="Dela Cruz"
						required
						value={lastName}
						onChange={event => {setLastName(event.target.value)}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="example@email.com"
						required
						value={email}
						onChange={event => {setEmail(event.target.value)}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Mobile Number:</Form.Label>
						<Form.Control type="tel" placeholder="+63 900 000 0000" required
						value={mobileNo}
						onChange={event => {setMobileNo(event.target.value)}}
						/>
						{
							isMobileValid ? 
								<span className="text-success">Mobile number is valid!</span>
							:
								<span className="text-secondary">Mobile number must be 11-digits.
								</span> 	  	
						}
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter your password"
						required
						value={password1}
						onChange={event => {setPassword1(event.target.value)}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm Password:</Form.Label>
						<Form.Control type="password" placeholder="Re-enter your password"
						required
						value={password2}
						onChange={event => {setPassword2(event.target.value)}}
						/>
						{
							isMatched ? 
								<span className="text-success">Passwords Matched!</span>
							:
								<span className="text-danger">Passwords Should Match!
								</span> 	  	
						}
						
					</Form.Group>
					{
						isActive ?
							<Button className="btn-primary btn-block mt-4" type="submit">Register
							</Button>
							:
							<Button className="btn-primary btn-block mt-4" disabled>Register
							</Button>
					}
					
				</Form>
			</Container>
		</>	
	);
};