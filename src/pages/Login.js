import Hero from '../components/Banner';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

const data = {
	title: 'Welcome to Login',
	content: 'Sign in your account below.'
}

export default function Login () {
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isEmailValid, setIsEmailValid] = useState(false);
	let addressSign = email.search('@');
	let dns = email.search('.com');


	useEffect(() => {
		if (dns !== -1 && addressSign !== -1) {
			setIsEmailValid(true);
			if (password !== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsEmailValid(false);
			setIsActive(false);
		}
	},[email, password, addressSign, dns])


	const loginUser = async (eventSubmit) => {
		eventSubmit.preventDefault();
		
		fetch('https://agile-springs-51885.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			    email: email,
			    password: password
			})
		}).then(res => res.json()).then(dataJSON => {
			let token = dataJSON.access;
			if (typeof token !== 'undefined') {
				localStorage.setItem('accessToken',token);

				fetch('https://agile-springs-51885.herokuapp.com/users/id', {
				  headers: {
				      Authorization: `Bearer ${token}`
				  }
				})
				.then(res => res.json())
				.then(convertedData => {
				  // console.log(convertedData)
				  if (typeof convertedData._id !== "undefined") {
				    setUser({
				      id: convertedData._id,
				      isAdmin: convertedData.isAdmin
				    });
				    Swal.fire({
				    	icon: "success",
				    	title: 'Login Successful!',
				    	text: 'Welcome to your account.'
				    })
				  } else {
				    setUser({
				      id: null,
				      isAdmin: null
				    });
				  }
				});

				
			} else {
				Swal.fire({
					icon: "error",
					title: 'Login Error!',
					text: 'Invalid credentials. Contact Admin if problem persist.'
				})
			}
		})
		
	
	};

	return (
		user.id
		?
			<Navigate to="/courses" replace={true}/>
		:
		<>
			<Hero bannerData={data}/>
			<Container>
				<h1 className="text-center">Login Form</h1>
				<Form onSubmit={e => loginUser(e)}>
					{/*Emaill Address*/}
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="example@email.com"
						required
						value={email}
						onChange={event => {setEmail(event.target.value)}}
						/>
						{
							isEmailValid ? 
								<span className="text-success">Email is valid!</span>
							:
								<span className="text-danger">Email is invalid!
								</span> 	  	
						}
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter your password"
						required
						value={password}
						onChange={event => {setPassword(event.target.value)}}
						/>
					</Form.Group>
					{
						isActive ?
							<Button className="btn-primary btn-block mt-4" type="submit">Login
							</Button>
							:
							<Button className="btn-primary btn-block mt-4" disabled>Login
							</Button>
					}
				</Form>
			</Container>
		</>
	);
};