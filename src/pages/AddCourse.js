import Hero from '../components/Banner';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Welcome to the Add Course Page',
	content: 'Fill in the form below with course details.'
}

export default function AddCourse () {

	const addCourse = (eventSubmit) => {
		eventSubmit.preventDefault();
		return(
			Swal.fire({
				icon: "success",
				title: 'Add Course Successful!',
				text: 'A new course was successfully created.'
			})
		);
	};

	return (
		<div>
			<Hero bannerData={data}/>
			
			<Container>
				<h1 className="text-center">Add Course Form</h1>
				<Form onSubmit={e => addCourse(e)}>
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" required />
					</Form.Group>
					
					<Button className="btn-primary btn-block mt-4" type="submit">Submit</Button>
				</Form>
			</Container>
		</div>	
	);
};