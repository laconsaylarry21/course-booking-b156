import Hero from '../components/Banner';
import {Col, Card, Button, Container} from 'react-bootstrap';
import {Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useState, useEffect } from 'react';
const data = {
	title: 'Welcome to B156 Booking-App',
	content: 'Check out our school campus.'
}

export default function CourseView () {

	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	/*console.log(useParams());*/
	const {id} = useParams()
	/*console.log(id);*/

	useEffect(() => {
		fetch(`https://agile-springs-51885.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData => {
			console.log(convertedData);

			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id])

	const enroll = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'Enrollment Successful!',
				text: 'Thank you for enrolling to this course.'
			})
		);
	};

	return (
		<div>
			<Hero bannerData={data}/>
			<Col>
				<Container>
					<Card className="text-center">
						<Card.Body>
							{/*Course Name*/}
							<Card.Title>
								<h3 className="mb-2">{courseInfo.name}</h3>
							</Card.Title>
							{/*Course Description*/}
							<Card.Subtitle>
								<h5>Description</h5>
							</Card.Subtitle>
							<Card.Text>{courseInfo.description}
							</Card.Text>
							<Card.Subtitle>
								<h5>Price</h5>
							</Card.Subtitle>
							<Card.Text>PHP {courseInfo.price}
							</Card.Text>
						</Card.Body>
						<Button variant="warning" className="btn-block" onClick={enroll}>Enroll</Button>
						<Link className="btn btn-success btn-block mb-3" to="/login">Login to Enroll</Link>
					</Card>
				</Container>
			</Col>
		</div>
	);
};