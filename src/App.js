import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Catalog from './pages/Courses';
import AddCourse from './pages/AddCourse';
import UpdateCourse from './pages/UpdateCourse';
import ErrorPage from './pages/Error';
import CourseView from './pages/CourseView';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  }

  useEffect(() => {
    // console.log(user);
    let token = localStorage.getItem('accessToken');
    // console.log(token);

    fetch('https://agile-springs-51885.herokuapp.com/users/id', {
      headers: {
          Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData => {
      // console.log(convertedData)
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });
    
  },[user]);



  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/courses' element={<Catalog />} />
          <Route path='/add-course' element={<AddCourse />} />
          <Route path='/update-course' element={<UpdateCourse />} />
          <Route path='/courses/view/:id' element={<CourseView />} />
          <Route path='*' element={<ErrorPage />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
