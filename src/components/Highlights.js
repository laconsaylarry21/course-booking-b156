import {Row, Col, Card, Container} from 'react-bootstrap'

export default function Highlights() {
	return(
		<Container>
			<Row className="my-3">
				{/*1st Hightlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Learm From Home </Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, debitis!</Card.Text>
						</Card.Body>
						
					</Card>
				</Col>
				{/*2nd Hightlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Study Now, Pay Later </Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, debitis!</Card.Text>
						</Card.Body>
						
					</Card>
				</Col>
				{/*3rd Hightlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Job Hunting Assistance </Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, debitis!</Card.Text>
						</Card.Body>
						
					</Card>
				</Col>
			</Row>
		</Container>
		
	)
}