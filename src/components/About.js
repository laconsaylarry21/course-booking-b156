import {Row, Col} from 'react-bootstrap';

export default function About() {
	return(
		<Row className="p-5 abtPage">
			<Col>
				<h1 className="mb-4 border-bottom-1"> About Me</h1>
				<h3>Larry Laconsay</h3>
				<h5>Full Stack Web Developer</h5>
				<p className="my-2">I'm a career-shifter - a former BPO employee who wants to pursue a career in the IT industry as a Full-stack Web Developer.</p>
				<h5>Contact Me</h5>
				<ul>
					<li>Email: laconsaylarry21@gmail.com</li>
					<li>Mobile No: 09459734451</li>
					<li>Address: San Juan, La Union, Philippines 2514</li>
				</ul>

			</Col>
		</Row>
	);
}
