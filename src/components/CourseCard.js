import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	return (
		
				<Card className="cardForm m-4 d-md-inline-flex d-sm-inline-flex">				
					<Card.Body>
						<Card.Title>
							{courseProp.name}
						</Card.Title>
						<Card.Text>
							{courseProp.description}
						</Card.Text>
						<Card.Text>
							{courseProp.price}
						</Card.Text>
						<Link to={`view/${courseProp._id}`}class="btn btn-primary">
							View Course
						</Link>
					</Card.Body>
				
				</Card>
	)
}